.\" -*-nroff-*-
.\"
.\"     Copyright (C) 1998-2000 Hugo Haas <hugo@via.ecp.fr>
.\"
.\"     This program is free software; you can redistribute it and/or modify
.\"     it under the terms of the GNU General Public License as published by
.\"     the Free Software Foundation; either version 2 of the License, or
.\"     (at your option) any later version.
.\"
.\"     This program is distributed in the hope that it will be useful,
.\"     but WITHOUT ANY WARRANTY; without even the implied warranty of
.\"     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\"     GNU General Public License for more details.
.\"
.\"     You should have received a copy of the GNU General Public License
.\"     along with this program; if not, write to the Free Software
.\"     Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
.\"
.TH IPPL.CONF 5 "Last change: 11 February 2000"

.SH NAME

ippl.conf \- IP Protocols Logger configuration file

.SH DESCRIPTION

The
.I ippl.conf
file is the only configuration file for the
.BR ippl
logger. It defines what protocols to log, and the kind of packets to
log.

A hash mark (``#'') indicates that the end of the line is a comment
and it will therefore not be read.

.SH USER RUNNING THREADS

.BR ippl
does not run (unless specified) the protocol logging threads as root 
for security reasons. You can specify which user should be use with the
.I runas
keyword.

.B Syntax:
runas [user]
.PP
.I user
is a user defined in /etc/passwd. By default, the __DEFAULT_USER
user is used.

.SH PROTOCOLS

Each protocol is run by an different thread. To run a thread, use the:

.B Syntax:
run [protocol] [protocol] ...
.PP
.I protocol 
can be:

.I icmp
to specify that the thread logging ICMP messages should be run.
.PP
.I tcp
to specify that the thread logging TCP connections should be run.
.PP
.I udp
to specify that the thread logging UDP datagrams should be run.
.PP
.I all
to log all the protocols.

.SH ADDRESS RESOLUTION

You can enable or disable IP address resolution on a protocol basis.
To enable address resolution, use:

.B Syntax:
resolve [protocol] [protocol] ...
.PP
.I protocol
is the same as in the protocols section.

To disable address resolution, use:

.B Syntax:
noresolve [protocol] [protocol] ...
.PP
.I protocol
is the same as before.
.PP
By default, IP address resolution is disabled for all the protocols.

.SH LOGGING FORMAT

.BR ippl
can log IP protocols in a more or less detailed format. By default, it
only shows the source address and the type or the destination port. A
more detailed version can be used. There is also a shortest version.

.B Syntax:
logformat [format] [protocol] [protocol] ...
.PP
.I format
can be:

.I short
to use a short format for logging.
.PP
.I normal
to use the normal format. This is the default.
.PP
.I detailed
to log more information. This option displays the source and
destination ports and addresses.

.PP
.I protocol
is the same as in the protocols section.

.SH IDENT MECHANISM

To enable the IDENT remote username resolution, use the
.I ident
keyword.
To disable it, use the
.I noident
keyword.
Note that the information returned is *NOT* reliable in general since
it is returned by the remote host. By default, the ident resolution is
off.

.SH TCP CONNECTION TERMINATION

.BR ippl
can detect when a TCP connection is closed. To enable this feature,
use the
.I logclosing
keyword.
To disable it, use the
.I nologclosing
keyword.
By default, TCP connection terminations are ignored.

.SH LOGGING MECHANISM

.BR ippl
can log messages using syslog (using the LOG_DAEMON facility) or it
can write directly into a file. This is specified using
.I log\-in
keyword.

.B Syntax:
log-in [protocol] [filename]
.PP
.I protocol
is the same as in the protocols section.
.I filename
is an absolute path to a file. Note that the file cannot be in the
root directory; it has to be in a directory.

NOTE: when the logs are rotated,
.BR ippl
opens new files when it is sent the SIGHUP signal.

.SH RULES

When a thread is run, it will catch all the packets using the protocol
logged. The user may want to ignore certain packets. This is done with
Apache-like rules.
.PP
There are two different types of rules. The first one describes what
packets to log, and the second one describes the packets that should
be ignored. The syntax of a rule is as follows:

.B Syntax:
[log|ignore] {option [option],[option],...} [protocol] [description]

.I log
means that the packets described should be logged and
.I ignore
is used if the user does not want to log a certain type of packets.

.SS Option
.PP
The
.I option
keyword will permit to override the default values for this rule only.
.I options
is also recognized.
.PP
Valid options are:
.PP
.I resolve
enable IP address resolution.
.PP
.I noresolve
disable IP address resolution.
.PP
.I ident
use ident logging (only for TCP).
.PP
.I noident
disable ident logging (only for TCP).
.PP
.I logclosing
log connection termination (only for TCP).
.PP
.I nologclosing
do not log connection termination (only for TCP).
.PP
.I short
use the short logging format.
.PP
.I normal
use the normal logging format.
.PP
.I detailed
use the detailed logging format.

.SS Protocol
.PP
protocol is one of the supported protocols (see the protocols
section), except the
.I all
keyword, which is not supported.

.SS Description
.PP
.I description
holds the type of packet and the hosts to which the rule
applies.
.PP
.I Type of packet:
.PP
   type <number>    Specify an ICMP message type.
   port <number>    Specify a destination TCP or UDP port number.
   port <name>      Specify a destination TCP or UDP port name.
   srcport <number> Specify a source TCP or UDP port number.
   srcport <name>   Specify a source TCP or UDP port name.
.PP
number is specified like this:
   n               Number n.
   n--             Every number m >= n.
   --n             Every number m <= n.
   l--k            Every number m, with l <= m <= k.
   string          If a string is specified, it is
                     either the name of a service
                     (see /etc/services) or an
                     ICMP message.
                   Keywords for ICMP messages are:
                     echo_reply      0
                     dest_unreach    3
                     src_quench      4
                     redirect        5
                     echo_req        8
                     router_advert   9
                     router_solicit  10
                     time_exceeded   11
                     param_problem   12
                     ts_req          13
                     ts_reply        14
                     info_req        15
                     info_reply      16
                     addr_mask_req   17
                     addr_mask_reply 18
.PP
.I Source of the packets:
.PP
   from <host>
.PP
where host is specifed as follows:
   x.x.x.x         IP address of a host
   x.x.x.x/x.x.x.x IP address, followed by a network mask to specify a
subnet
   x.x.x.x/n       IP address, followed by the number of 1's at the left side of the network mask
   host.net.domain host name (wildcards accepted)
.PP
.I Destination of the packets:
.PP
   to <host>
.PP
where host is specified as follows:
   x.x.x.x          IP address of the local interface
   host.net.domain  host name of the local interface (*no* wildcards accepted)
.PP
This rule is useful only if you have multiple interfaces connected
to your box, or if you use IP aliasing. This can also be useful if you want
to log or ignore broadcasts. To do so, just use your broadcast address as
destination IP address.
.PP
Please note that rules using IP addresses are faster to check than
rules using host names.
.PP
If you log UDP, it is *strongly* recommended to ignore the broadcasts!
(until we implement an option for that).

.SH EXPIRATION OF DNS CACHE

The time for which
.BR ippl
holds cached DNS data without performing any queries can be changed.

.B Syntax:
expire <time>

defines how often the DNS data expires.
.I time
is specified in seconds (default is 3600).

.SH FILES
 __CONFIGURATION_FILE - configuration file
 /usr/share/doc/ippl/* - files worth reading if you still have a question

.SH SEE ALSO
ippl(8)

.SH AUTHORS
Hugo Haas (hugo@larve.net)
Etienne Bernard (eb@via.ecp.fr)
