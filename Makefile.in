#
# Makefile for IP protocol logger
#

include ./Makefile.common

# Commands
INSTALL=@INSTALL@
RM=rm -f

all: binary docs

binary: Makefile Source/Makefile
	@cd Source && make && cd ..

docs:
	@cd Docs && make && cd ..

install: all
	$(INSTALL) -d -m 755 $(SBINDIR)
	$(INSTALL) -s -m 755 Source/ippl $(SBINDIR)/ippl
	$(INSTALL) -d -m 755 $(ETCDIR)
	[ -f $(CONFIGURATION_FILE) ] || \
	  $(INSTALL) -m 644 ippl.conf $(CONFIGURATION_FILE)
	$(INSTALL) -d -m 755 $(MANDIR)
	$(INSTALL) -d -m 755 $(MANDIR)/man5
	$(INSTALL) -d -m 755 $(MANDIR)/man8
	$(INSTALL) -m 644 Docs/ippl.8 $(MANDIR)/man8/ippl.8
	$(INSTALL) -m 644 Docs/ippl.conf.5 $(MANDIR)/man5/ippl.conf.5

clean:
	@cd Source && make clean && cd .. && cd Docs && make clean && cd ..

distclean: clean
	$(RM) *~ Makefile.common Makefile Source/Makefile Docs/Makefile build-stamp install-stamp

mrproper: distclean
	$(RM) config.log config.cache config.status && autoconf

configdump:
	@echo "Configuration:" && \
	echo "   ippl in ${SBINDIR}" && \
	echo "   man pages in ${MANDIR}" && \
	echo "   configuration file in ${ETCDIR}" && \
	echo "   pid file in ${VARRUN}" && \
	echo "   using ${DEFAULT_USER} account to run threads"
