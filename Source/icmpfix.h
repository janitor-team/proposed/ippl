/*
 *  icmpfix.h - definitions for libc5 compatibility
 *
 *  Copyright (C) 1999 Hugo Haas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#define ICMP_UNREACH_NET ICMP_NET_UNREACH
#define ICMP_UNREACH_HOST ICMP_HOST_UNREACH
#define ICMP_UNREACH_PROTOCOL ICMP_PROT_UNREACH
#define ICMP_UNREACH_PORT ICMP_PORT_UNREACH
#define ICMP_UNREACH_NEEDFRAG ICMP_FRAG_NEEDED
#define ICMP_UNREACH_SRCFAIL ICMP_SR_FAILED
#define ICMP_UNREACH_NET_UNKNOWN ICMP_NET_UNKNOWN
#define ICMP_UNREACH_HOST_UNKNOWN ICMP_HOST_UNKNOWN
#define ICMP_UNREACH_ISOLATED ICMP_HOST_ISOLATED
#define ICMP_UNREACH_NET_PROHIB ICMP_NET_ANO
#define ICMP_UNREACH_HOST_PROHIB ICMP_HOST_ANO
#define ICMP_UNREACH_TOSNET ICMP_NET_UNR_TOS
#define ICMP_UNREACH_TOSHOST ICMP_HOST_UNR_TOS
#define ICMP_UNREACH_FILTER_PROHIB ICMP_PKT_FILTERED
#define ICMP_UNREACH_HOST_PRECEDENCE ICMP_PREC_VIOLATION
#define ICMP_UNREACH_PRECEDENCE_CUTOFF ICMP_PREC_CUTOFF
#define ICMP_REDIRECT_NET ICMP_REDIR_NET
#define ICMP_REDIRECT_HOST ICMP_REDIR_HOST
#define ICMP_REDIRECT_TOSNET ICMP_REDIR_NETTOS
#define ICMP_REDIRECT_TOSHOST ICMP_REDIR_HOSTTOS
