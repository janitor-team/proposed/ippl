/*
 *  netutils.h - functions common to all the protocols
 *
 *  Copyright (C) 1998-1999 Hugo Haas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef NETUTILS_H
#define NETUTILS_H

#include "defines.h"
#include <netinet/ip.h>

#define HOST_LENGTH 512

#define DNS_EXPIRE 3600

#define MAX_IPHDR_LENGTH (((1 << 4) - 1) << 2)

/* Hashing parameters */
/* Open addressing, double hashing */
/* Hugo thanks Cormen, Leiserson & Rivest :-) */

#define TABLE_SIZE 257

#define LEVELS 3

#define HASH1(n) ((n) % TABLE_SIZE)
#define HASH2(n) (1 +((n) % (TABLE_SIZE - 2)))

/* Display whether IP options are specified */
#define IPOPTIONS(p) (((struct iphdr *) p)->ihl == 5) ? "" : " (IP opts)"

int host_lookup(char *host, __u32 in);
void initialize_dns_cache(void);

void host_print(char *hostname, __u32 in, int resolve);

void get_details(char *details,
                 const __u32 src_addr, const __u16 src_port,
                 const __u32 dst_addr, const __u16 dst_port);

void service_lookup(char *proto, char *service, __u16 port);

#endif
