Source: ippl
Section: net
Priority: optional
Maintainer: Debian ippl maintainers <ippl@packages.debian.org>
Uploaders: Marc Haber <mh+debian-packages@zugschlus.de>,
           RISKO Gergely <risko@debian.org>
Build-Depends: bison, debhelper-compat (= 13), flex
Standards-Version: 4.6.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/debian/ippl.git
Vcs-Browser: https://salsa.debian.org/debian/ippl


Package: ippl
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: adduser (>> 3.51),
         logrotate,
         lsb-base (>= 3.0-6),
         ${misc:Depends},
         ${shlibs:Depends}
Description: IP protocols logger
 writes information about incoming ICMP messages, TCP connections and
 UDP datagrams to syslog.
 .
 It is highly configurable and has a built-in DNS cache.
 .
 Please note that upstream is rather inactive lately (no release since
 2001), and that there are some rather nasty bugs.
 .
 An incomplete list of the bugs includes:
   - random packets don't get logged sometimes
   - stops logging at all after some weeks
   - ipv6 never got implemented
   - documentation is out of sync.
 .
 Trying to fix these bugs is not easy. Please do not expect the Debian
 maintainer to do this, but patches are appreciated.
 .
 Please consider using a fully-grown intrusion detection system (like
 snort) instead of ippl.
